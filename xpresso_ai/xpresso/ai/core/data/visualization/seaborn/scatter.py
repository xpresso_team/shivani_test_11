import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    InputLengthMismatch
from xpresso.ai.core.commons.utils.constants import DEFAULT_SUBPLOT_COLUMNS
from xpresso.ai.core.data.visualization import utils
from xpresso.ai.core.data.visualization.seaborn.res.plot_factory import Plot


class NewPlot(Plot):
    """
    Generates a scatter plot
    Attributes:
        plot(:obj: matplotlib.axes): Matplotlib axes which stores the plot
    Args:
        input_1(list): X axis data
        input_2(list): Y axis data
        target(bool): If target present or not. If true plots a
            regression line
        plot_title(str): Title to the plot
        output_format (str): html or png plots to be generated
        file_name(str): File name of the plot to be stored
        axes_labels(dict): Dictionary of x_label, y_label and
            target_label
        output_path(str): path where the html/png plots to be stored
    """

    def __init__(self, input_1, input_2, target=False, plot_title=None,
                 output_format=utils.HTML,
                 file_name=None, axes_labels=None,
                 output_path=utils.DEFAULT_IMAGE_PATH, auto_render=False):
        super().__init__()
        sns.set()
        if axes_labels:
            self.axes_labels = axes_labels
        if plot_title:
            self.plot_title = plot_title
        if len(input_1) != len(input_2):
            raise InputLengthMismatch()
        # Process input data
        data = pd.DataFrame(list(zip(input_1, input_2)))
        self.plot = sns.scatterplot(x=0, y=1, data=data)
        self.plot.set_ylim(data[1].min(), data[1].max())
        self.plot.set_xlim(data[0].min(), data[0].max())
        if target:
            self.plot = sns.regplot(x=data[0], y=data[1], data=data)
        self.plot.set(xlabel=self.axes_labels[utils.X_LABEL],
                      ylabel=self.axes_labels[utils.Y_LABEL],
                      title=self.plot_title)
        if auto_render:
            self.render(output_format=output_format, output_path=output_path,
                        file_name=file_name)


class Join(Plot):
    """
    Generates multiple scatter plot
    Args:
        input_1(:obj: Dataframe): X axis data
        input_2(:obj: Dataframe): Y axis data multiple numeric columns
        target(bool): If target a regression line is plotted.
        plot_title(str): Title to the plot
        output_format (str): html or png plots to be generated
        file_name(str): File name of the plot to be stored
        output_path(str): path where the html/png plots to be stored
    """

    def __init__(self, input_1, input_2, target=None, output_format=utils.HTML,
                 file_name=None, output_path=utils.DEFAULT_IMAGE_PATH,
                 plot_title=None):
        super().__init__()
        sns.set(font_scale=1.5)
        data_1 = pd.DataFrame(input_1)
        data_2 = pd.DataFrame(input_2)
        column_names = data_2.columns
        num_columns = DEFAULT_SUBPLOT_COLUMNS
        num_rows = DEFAULT_SUBPLOT_COLUMNS
        self.plot, self.axes = plt.subplots(num_rows, num_columns,
                                            figsize=self.figure_size)
        row = num_rows
        col = num_columns
        for subplot_index in range(len(column_names)):
            row = int(subplot_index / num_columns) % num_rows
            col = subplot_index % num_columns
            self.axes[row, col].scatter(x=data_1[:],
                                        y=data_2[column_names[subplot_index]])
            if target:
                sns.regplot(x=data_1[:], y=data_2[
                    column_names[subplot_index]], ax=self.axes[row, col])
            self.axes[row, col].set(xlabel=data_1.columns[0],
                                    ylabel=column_names[subplot_index])
            if subplot_index % 4 == 0 and subplot_index != 0:
                self.plot.suptitle(plot_title)
                self.render(output_format=output_format,
                            output_path=output_path,
                            file_name=file_name)
                self.plot, self.axes = plt.subplots(num_rows, num_columns,
                                                    figsize=self.figure_size)
        del_col = (col + 1) % num_columns
        del_row = row
        if del_col == 0:
            del_row = del_row + 1
        while del_row < num_columns:
            self.plot.delaxes(self.axes[del_row, del_col])
            del_col = (del_col + 1) % num_columns
            if del_col == 0:
                del_row += 1
        self.render(output_format=output_format, output_path=output_path,
                    file_name=file_name, bbox_inches="tight")
